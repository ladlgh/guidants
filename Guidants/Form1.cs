﻿using Gecko;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Guidants
{
    public partial class Form : System.Windows.Forms.Form
    {
        public Form()
        {
            InitializeComponent();

            // set the path of the directory where the firefox files are located
            string xrPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            xrPath = xrPath.Substring(0, xrPath.LastIndexOf(@"\") + 1) + "Firefox";

            // initialize the path
            Xpcom.Initialize(xrPath);
        }

        private void Form_Load(object sender, EventArgs e)
        {
            string path = "https://go.guidants.com/#";

            WebBrowser.Navigate(path);
        }

        private void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            // dispose the web browser control
            WebBrowser.Dispose();
            WebBrowser = null;

            // shutdown the XULRunner services
            Xpcom.Shutdown();
        }
    }
}
